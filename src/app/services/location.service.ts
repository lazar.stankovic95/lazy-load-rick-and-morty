import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Endpoints } from '../endpoints';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class LocationService {
  constructor(private _apiService: ApiService) {}

  getLocations = (): Observable<any> => {
    return this._apiService.get(Endpoints.location);
  };
}
