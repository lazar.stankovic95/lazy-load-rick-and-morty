import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Endpoints } from '../endpoints';
import { HttpClient } from '@angular/common/http';
import { endpointMaker } from '../helpers/utils';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private _httpClient: HttpClient) {}

  get = (endpointURL: Endpoints): Observable<any> => {
    return this._httpClient.get(endpointMaker(endpointURL));
  };

  post = () => {
    return;
  };

  put = () => {
    return;
  };

  delete = () => {
    return;
  };
}
