import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Endpoints } from '../endpoints';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class EpisodeService {
  constructor(private _apiService: ApiService) {}

  getEpisodes = (): Observable<any> => {
    return this._apiService.get(Endpoints.episode);
  };
}
