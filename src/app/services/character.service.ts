import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Endpoints } from '../endpoints';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class CharacterService {
  constructor(private _apiService: ApiService) {}

  getCharacters = (): Observable<any> => {
    return this._apiService.get(Endpoints.character);
  };
}
