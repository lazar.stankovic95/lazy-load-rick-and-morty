import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'characters',
    loadChildren: () =>
      import('src/app/pages/characters/characters.module').then(
        (m) => m.CharactersModule
      ),
  },
  {
    path: 'episodes',
    loadChildren: () =>
      import('src/app/pages/episodes/episodes.module').then(
        (m) => m.EpisodessModule
      ),
  },
  {
    path: 'locations',
    loadChildren: () =>
      import('src/app/pages/locations/locations.module').then(
        (m) => m.LocationsModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
