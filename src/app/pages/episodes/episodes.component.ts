import { Component, OnInit } from '@angular/core';
import { EpisodeService } from 'src/app/services/episode.service';

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss'],
})
export class EpisodesComponent implements OnInit {
  constructor(private _episodesAPI: EpisodeService) {}
  isLoading: boolean = false;
  episodes: any;
  ngOnInit(): void {
    this.isLoading = true;
    this._episodesAPI
      .getEpisodes()
      .toPromise()
      .then((response) => {
        response ? (this.episodes = response.results) : (this.episodes = null);
        this.isLoading = false;
      })
      .catch((err) => {
        alert(err);
      });
  }
}
