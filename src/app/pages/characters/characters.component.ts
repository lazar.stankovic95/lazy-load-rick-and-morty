import { Component, OnInit } from '@angular/core';
import { CharacterService } from 'src/app/services/character.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss'],
})
export class CharactersComponent implements OnInit {
  characters: any;
  isLoading: boolean = false;
  constructor(private _charactersAPI: CharacterService) {}

  ngOnInit(): void {
    this.isLoading = true;
    this._charactersAPI
      .getCharacters()
      .toPromise()
      .then((response) => {
        response
          ? (this.characters = response.results)
          : (this.characters = null);
        this.isLoading = false;
      })
      .catch((err) => {
        alert(err);
      });
  }
}
