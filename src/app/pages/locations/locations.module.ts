import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationsComponent } from './locations.component';
import { LocationsRoutingModule } from './location-routing.module';
import { LocationService } from 'src/app/services/location.service';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [LocationsComponent],
  imports: [
    CommonModule,
    LocationsRoutingModule,
    MatCardModule,
    MatDividerModule,
    FlexLayoutModule,
  ],
  providers: [LocationService],
})
export class LocationsModule {}
