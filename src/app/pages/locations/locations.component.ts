import { Component, OnInit } from '@angular/core';
import { LocationService } from 'src/app/services/location.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
  constructor(private _locationsAPI: LocationService) {}
  isLoading: boolean = false;
  locations: any;
  ngOnInit(): void {
    this.isLoading = true;
    this._locationsAPI
      .getLocations()
      .toPromise()
      .then((response) => {
        response
          ? (this.locations = response.results)
          : (this.locations = null);
        this.isLoading = false;
      })
      .catch((err) => {
        alert(err);
      });
  }
}
