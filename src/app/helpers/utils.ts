import { Endpoints } from '../endpoints';
const BASE_URL: string = 'https://rickandmortyapi.com/api';

export const endpointMaker = (endpointUrl: Endpoints): string => {
  return BASE_URL + endpointUrl;
};
