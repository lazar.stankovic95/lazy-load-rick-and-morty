export enum Endpoints {
  character = '/character',
  location = '/location',
  episode = '/episode',
}
